# Bark
The HTML5/JS Dock for Linux Desktops

[![Bark In Action](http://i.imgur.com/WwuQ4dz.jpg)](http://i.imgur.com/WwuQ4dz.jpg)

This project aims to bring modern UI/UX technologies to the Linux desktop. By leveraging tried and true web technologies, we'll be able to bring a high level of customization and flexibility to the Linux desktop without sacrificing in security, stability, or performance.

This repository intentionally left empty, please check back in the beginning of September when the core author has committed his code!